<?php

use Illuminate\Support\Facades\File;

if(!function_exists('addImage')){
    /**
     * @param $file
     * @return string|null
     */
     function addImage($file){
//         dd($file);
        if($file){
//            dd("entered");
            $destinationPath = 'image/';
            $profileImage = $file->getATime()."-". $file->getclientoriginalname();
            $file->move($destinationPath, $profileImage);
//            dd($file->move($destinationPath, $profileImage));
            return $destinationPath . $profileImage;
        }

        return null;
    }
}

if (!function_exists('deleteImage')){
    /**
     * @param String $photoUrl
     * @return bool|null
     */
    function deleteImage(String $photoUrl){
//       dd( public_path($photoUrl));
        if(File::exists(public_path($photoUrl))){
            return File::delete(public_path($photoUrl));
        }
        return null;
    }
}
