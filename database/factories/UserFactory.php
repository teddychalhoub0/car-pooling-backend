<?php

namespace Database\Factories;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $model = User::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=> $this->faker->firstNameFemale(),
            'username'=>$this->faker->userName(),
            'email'=>$this->faker->email(),
            'password'=>bcrypt("123"),
            'mobile'=>$this->faker->phoneNumber(),
            'about_me'=>$this->faker->country()
        ];
    }
}
