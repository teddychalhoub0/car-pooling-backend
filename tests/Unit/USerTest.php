<?php

namespace Tests\Unit;

use Tests\TestCase;

class USerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_user_login()
    {
        $response = $this->post("/api/users/login",
            ['email'=>'teddy@email.com','password'=>'T@1E@2D@3teddy']);
        $response->assertJsonFragment(['message'=>'Login successfully','success'=>true]);
    }

    /**
     * testing login if failed
     *
     * @return void
     */
    public function test_user_login_failed()
    {
        $response = $this->post("/api/users/login",
            ['email'=>'teddy@email.com','password'=>'T@1E@D@3teddy']);
        $response->assertJsonFragment(['message' => 'Wrong username or password']);
    }
}
