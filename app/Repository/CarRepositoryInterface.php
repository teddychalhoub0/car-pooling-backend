<?php
namespace App\Repository;
use Illuminate\Database\Eloquent\Model;

interface CarRepositoryInterface{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function findByUserId($id);

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id):?Model;

    /**
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int;
}
