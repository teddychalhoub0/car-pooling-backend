<?php
namespace App\Repository;

interface PostRepositoryInterface{
    /**
     * @return mixed
     */
    public function all();

    /**
     *
     * Delete posts related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int;

    /**
     * @param $id
     * @return int|null
     */
    public function findUserIdToPost($id): ?int;

    /**
     * @param $value
     * @return mixed
     */
    public function searchByDeparture($value);

    /**
     * @param $value
     * @return mixed
     */
    public function searchByDestination($value);

}
