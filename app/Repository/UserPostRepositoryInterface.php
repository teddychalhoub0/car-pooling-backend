<?php
namespace App\Repository;
use Illuminate\Database\Eloquent\Model;

interface UserPostRepositoryInterface{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function getUserPostsByUserId($id);

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model;

    /**
     * @param $id
     * @return mixed
     */
    public function getByUserIdPostRelated($id);

    /**
     * @param $id
     * @return int|null
     */
    public function deleteRelatedToPost($id): ?int;

    /**
     * @param $id
     * @return int|null
     */
    public function deleteRelatedToUser($id): ?int;
}
