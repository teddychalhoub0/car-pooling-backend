<?php
namespace App\Repository;
interface AdminRepositoryInterface{
    /**
     * @return mixed
     */
    public function all();
}
