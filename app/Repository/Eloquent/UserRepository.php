<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends BaseRepository implements UserRepositoryInterface{
    /**
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all()
    {
        return $this->model->with("posts","user_post.post","locations",'cars')->get();
    }

    public function find($id): ?Model
    {
        return $this->model->where("id",$id)->with("posts","user_post.user","user_post.post.user","locations",'cars')->first();
    }
}
