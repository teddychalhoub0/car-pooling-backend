<?php

namespace App\Repository\Eloquent;

use App\Models\Car;
use App\Repository\CarRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CarRepository extends BaseRepository implements CarRepositoryInterface {
    /**
     * @param Car $model
     */
    public function __construct(Car $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all(){
        return $this->model->all();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function findByUserId($id)
    {
        return $this->model->where('user_id',$id)->get();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        return $this->model->where('id',$id)->with('user')->first();
    }

    /**
     *
     * Delete Car related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int
    {
        return $this->model->where('user_id',$id)->delete();
    }
}
