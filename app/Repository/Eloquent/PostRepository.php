<?php
namespace App\Repository\Eloquent;

use App\Models\Post;
use App\Repository\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class PostRepository extends BaseRepository implements PostRepositoryInterface{

    /**
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function all()
    {
        return $this->model->with('user')->get();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        return $this->model->where('id',$id)->with('user')->first();
    }

    /**
     *
     * Delete posts related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int
    {
        return $this->model->where('user_id',$id)->delete();
    }

    /**
     * @param $id
     * @return int|null
     */
    public function findUserIdToPost($id): ?int
    {
        return $this->model->where('id',$id)->pluck("user_id")->first();
    }


    /**
     *
     * search for posts depending on departure place
     *
     * @param $value
     * @return mixed
     */
    public function searchByDeparture($value)
    {
        return $this->model->where('departure_place','LIKE',"%$value%")->with('user')->get();
    }

    /**
     *
     * search for posts depending on destination place
     *
     * @param $value
     * @return mixed
     */
    public function searchByDestination($value)
    {
        return $this->model->where('destination_place','LIKE',"%$value%")->with('user')->get();
    }
}
