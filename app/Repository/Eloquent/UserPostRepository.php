<?php
namespace App\Repository\Eloquent;

use App\Models\UserPost;
use App\Repository\UserPostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserPostRepository extends BaseRepository implements UserPostRepositoryInterface{
    /**
     * @param UserPost $model
     */
    public function __construct(UserPost $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all(){
        return $this->model->with('user','post.user')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserPostsByUserId($id){
        return $this->model->where('user_id',$id)->with('user','post.user')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByUserIdPostRelated($id){
        return $this->model->whereHas('post',function ($q) use($id){
            $q->where('user_id',$id);
        })->with('user','post.user')->get();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        return $this->model->where('id',$id)->with('user','post.user')->first();
    }

    /**
     *
     * delete user post using post id
     *
     * @param $id
     * @return int|null
     */
    public function deleteRelatedToPost($id): ?int
    {
        return $this->model->where('post_id',$id)->delete();
    }

    /**
     *
     * delete user post using user id
     *
     * @param $id
     * @return int|null
     */
    public function deleteRelatedToUser($id): ?int
    {
        return $this->model->where('user_id',$id)->delete();
    }
}
