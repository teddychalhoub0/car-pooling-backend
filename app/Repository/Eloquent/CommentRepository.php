<?php

namespace App\Repository\Eloquent;

use App\Models\Comment;
use App\Repository\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface{
    /**
     * @param Comment $model
     */
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all(){
        return $this->model->all();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function findByUserId($id)
    {
        return $this->model->where('user_id',$id)->get();
    }


    /**
     * @param $id
     * @return Model|null
     */
    public function findByPostId($id)
    {
        return $this->model->where('post_id',$id)->get();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        return $this->model->where('id',$id)->with('user','post')->first();
    }

    /**
     *
     * Delete Comment related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int
    {
        return $this->model->where('user_id',$id)->delete();
    }
}
