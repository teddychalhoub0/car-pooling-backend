<?php
namespace App\Repository\Eloquent;
use App\Models\Location;
use App\Repository\LocationRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class LocationRepository extends BaseRepository implements LocationRepositoryInterface{

    /**
     * @param Location $model
     */
    public function __construct(Location $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all(){
        return $this->model->all();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function findByUserId($id)
    {
        return $this->model->where('user_id',$id)->get();
    }

    /**
     *
     * Delete locations related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int
    {
        return $this->model->where('user_id',$id)->delete();
    }
}
