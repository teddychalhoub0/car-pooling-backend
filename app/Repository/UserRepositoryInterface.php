<?php
namespace App\Repository;
interface UserRepositoryInterface{

    /**
     * @return mixed
     */
    public function all();

}
