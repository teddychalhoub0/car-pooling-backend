<?php
namespace App\Repository;
interface LocationRepositoryInterface{

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function findByUserId($id);

    /**
     *
     * Delete locations related to a specific user
     *
     * @param $id
     * @return int|null
     */
    public function deleteByUserId($id): ?int;

}
