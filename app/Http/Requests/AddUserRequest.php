<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required'],
            'username'=>['required','unique:users'],
            'image'=>['mimes:jpeg,jpg,png,gif','max:10000'],
            'user_license'=>['mimes:jpeg,jpg,png,gif','max:10000'],
            'car_license'=>['mimes:jpeg,jpg,png,gif','max:10000'],
            'email'=>['required','unique:users','regex: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/'],
            'password'=>['required', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'],
            'mobile'=>['regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/']
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute should be provided',
            'unique'=>':attribute already exist',
            'image.max'=>'Image must be 9MB or less',
            'image.mimes'=>'Image must be of type jpeg,jpg,png or gif',
            'password.regex'=>'Password should contain at least one Uppercase, one Lowercase, one Numeric and one special character',
            'mobile.regex'=>'Mobile must be valid',
            'email.regex'=>'Wrong email format'
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'Name',
            'username'=>'Username',
            'image'=>'Image',
            'email'=>'Email',
            'password'=>'Password',
            'mobile'=>'Mobile',
            'car_license'=>'Car license'
        ];

    }

    protected function failedValidation(Validator $validator)
    {

        $errors = collect($validator->errors());
//        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Errors occured',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator,$response));

    }
}
