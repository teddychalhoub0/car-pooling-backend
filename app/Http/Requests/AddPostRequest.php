<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['required'],
            'image'=>['mimes:jpeg,jpg,png,gif','max:10000'],
            'content'=>['required'],
            'nb_of_pass'=>['required'],
            'departure_time'=>['required'],
            'estimated_time'=>['required'],
            'departure_place'=>['required'],
            'destination_place'=>['required'],

        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute should be provided',
            'user_id.required'=>'User should be choosen',
            'image.max'=>'Image must be 9MB or less',
            'image.mimes'=>'Image must be of type jpeg,jpg,png or gif'
        ];
    }

    public function attributes()
    {
        return [
            'title'=>'Title',
            'content'=>'Description',
            'nb_of_pass'=>'Number of passenger',
            'departure_time'=>'Departure time',
            'estimated_time'=>'Estimated time',
            'departure_place'=>'Departure place',
            'destination_place'=>'Destination place',
            'image'=>'Image',
            'user_id'=>'User'
        ];

    }

    protected function failedValidation(Validator $validator)
    {

        $errors = collect($validator->errors());
//        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Errors occured',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator,$response));

    }
}
