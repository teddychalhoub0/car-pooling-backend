<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'car_licence'=>['unique:posts,car_licence,'.$this->post],
            'image'=>['mimes:jpeg,jpg,png,gif','max:10000']
        ];
    }

    public function messages()
    {
        return [
            'unique'=>':attribute must be unique be provided',
            'user_id.required'=>'User should be chosen',
            'image.max'=>'Image must be 9MB or less',
            'image.mimes'=>'Image must be of type jpeg,jpg,png or gif'
        ];
    }

    public function attributes()
    {
        return [
            'title'=>'Title',
            'car_model'=>'Car model',
            'car_licence'=>'Car license',
            'content'=>'Description',
            'nb_of_pass'=>'Number of passenger',
            'departure_time'=>'Departure time',
            'estimated_time'=>'Estimated time',
            'departure_place'=>'Departure place',
            'destination_place'=>'Destination place',
            'user_id'=>'User'
        ];

    }

    protected function failedValidation(Validator $validator)
    {

        $errors = collect($validator->errors());
//        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Errors occured',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator,$response));

    }


}
