<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_plate_number'=>['required','unique:cars,car_plate_number'],
            'car_color'=>['required'],
            'car_model'=>['required'],
            'nb_seats'=>['required'],
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute should be provided',
            'unique'=>':attribute should be unique',
        ];
    }

    public function attributes()
    {
        return [
            'car_plate_number'=>'Car plate number',
            'car_color'=>'Car color',
            'car_model'=>'Car model',
            'nb_seats'=>'Number of seats',
        ];

    }

    protected function failedValidation(Validator $validator)
    {

        $errors = collect($validator->errors());
//        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Errors occured',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator,$response));

    }
}
