<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddUserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pickup_place'=>['required'],
            'post_id'=>['required']
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute should be provided',
            'pickup_place.required'=>':attribute should be chosen',
        ];
    }

    public function attributes()
    {
        return [
            'pickup_place'=>'Pickup place',
            'post_id'=>'Post',
            'user_id'=>'User id'
        ];

    }

    protected function failedValidation(Validator $validator)
    {

        $errors = collect($validator->errors());
//        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Errors occured',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator,$response));

    }
}
