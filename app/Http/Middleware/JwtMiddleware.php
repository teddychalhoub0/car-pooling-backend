<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next,$guard)
    {
        try {
            $user = auth($guard)->check();
            if( !$user ) throw new Exception('Not Found');
        } catch (Exception $e) {

            return response()->json([
                'data' => null,
                'success' => false,
                'message' => 'User must be logged in',
            ],401);
        }
        return $next($request);
    }

}
