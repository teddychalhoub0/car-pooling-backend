<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Models\Car;
use App\Repository\CarRepositoryInterface;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @param CarRepositoryInterface $carRepository
     */
    public function __construct(CarRepositoryInterface $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $userId = auth("users")->user()->id;
        $cars = $this->carRepository->findByUserId($userId);

        if(!$cars->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Cars retrieved successfully',
                'data'=>$cars

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No cars found',
                'data'=>$cars
            ],404);
        }
    }


    /**
     *
     * Store a newly created resource in storage.
     *
     * @param AddCarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddCarRequest $request)
    {

        $inputs = $request->all();
        $inputs['user_id'] = auth("users")->user()->id;

        if($path = addImage($request->file('image'))){
            $inputs['image'] = $path;
        }

        if($path2 = addImage($request->file('car_license'))){
            $inputs['car_license'] = $path2;
        }

        $car = $this->carRepository->create($inputs);

        if($car){

            return response()->json([
                "success" => 'true',
                'message' => 'Car created successfully',
                'data'=>$car
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Car wasn\'t created',
                'data'=>$car
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $car = $this->carRepository->find($id);

        if($car){
            return response()->json([
                "success" => 'true',
                'message' => 'Car retrieved successfully',
                'data'=>$car
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No car found',
                'data'=>$car
            ],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCarRequest $request, $id)
    {
        $inputs = $request->all();
        $oldCar = $this->carRepository->find($id);

        if($oldCar && isset($request->image)){
            if($oldCar->image){
                if(deleteImage($oldCar->image)){
                    if($path = addImage($request->file('image'))){

                        $inputs['image'] = $path;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Problem updating image'
                        ],400);
                    }
                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem updating image'
                    ],400);
                }
            }else{
                if($path = addImage($request->file('image'))){
                    $inputs['image'] = $path;

                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem uploading image'
                    ],400);
                }
            }
        }

        $car = $this->carRepository->update($inputs,$id);

        if($car){
            return response()->json([
                "success" => 'true',
                'message' => 'Car updated successfully',
                'data'=>$car
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Car wasn\'t updated',
                'data'=>$car

            ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $oldCar = $this->carRepository->find($id);

        if($oldCar && $oldCar->image){
            if(deleteImage($oldCar->image)){
                $car = $this->carRepository->delete($id);
            }else{
                return response()->json([
                    "success" => 'false',
                    'message' => 'Problem deleting image'
                ],400);
            }
        }else{
            $car = $this->carRepository->delete($id);
        }

        if($car){
            return response()->json([
                "success" => 'true',
                'message' => 'Car deleted successfully',
                'data'=>$car
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Car wasn\'t deleted',
                'data'=>$car

            ],400);
        }
    }
}
