<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Repository\CommentRepositoryInterface;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    /**
    * @var CommentRepositoryInterface
    */
    private CommentRepositoryInterface $commentRepository;

    /**
     * @param CommentRepositoryInterface $commentRepository
     */
    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        if(isset($request["post_id"]) && $request["post_id"] != ""){
            $comments = $this->commentRepository->findByPostId($request["post_id"]);

        }else{
            $comments = $this->commentRepository->all();

        }

        if(!$comments->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Comments retrieved successfully',
                'data'=>$comments

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Comments found',
                'data'=>$comments
            ],404);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCommentsByUserId(){

        $userId = auth("users")->user()->id;
        $comments = $this->commentRepository->findByUserId($userId);

        if(!$comments->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Comments retrieved successfully',
                'data'=>$comments

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Comments found',
                'data'=>$comments
            ],404);
        }
    }

    /**
     *
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth("users")->user()->id;
        $comment = $this->commentRepository->create($inputs);

        if($comment){

            return response()->json([
                "success" => 'true',
                'message' => 'Comment created successfully',
                'data'=>$comment
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Comment wasn\'t created',
                'data'=>$comment
            ],400);
        }
    }

    /**
     *
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $comment = $this->commentRepository->find($id);

        if($comment){
            return response()->json([
                "success" => 'true',
                'message' => 'Comment retrieved successfully',
                'data'=>$comment
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Comment found',
                'data'=>$comment
            ],404);
        }
    }


    /**
     *
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        $inputs = $request->all();

        $comment = $this->commentRepository->update($inputs,$id);

        if($comment){
            return response()->json([
                "success" => 'true',
                'message' => 'Comment updated successfully',
                'data'=>$comment
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Comment wasn\'t updated',
                'data'=>$comment

            ],400);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $comment = $this->commentRepository->delete($id);

        if($comment){
            return response()->json([
                "success" => 'true',
                'message' => 'Comment deleted successfully',
                'data'=>$comment
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Comment wasn\'t deleted',
                'data'=>$comment

            ],400);
        }
    }
}
