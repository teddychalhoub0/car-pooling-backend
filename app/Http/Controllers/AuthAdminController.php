<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthAdminController extends Controller
{
    public function login(){

        $credentials = request(['email', 'password']);

        if (!$token = auth('admins')->attempt($credentials)) {
            return response()->json([
                'success'=>false ,
                'message' => 'Wrong username or password'
            ], 401);
        }

        $admin = auth("admins")->user();

        return $this->createNewToken($token,$admin);
    }

    public function logout()
    {
        auth("admins")->logout();

        return response()->json([
            'success'=> true,
            'message' => 'Successfully logged out'
        ]);
    }

    protected function createNewToken($token,$admin){
        return response()->json([
            'success'=> true,
            'data'=>$admin,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
