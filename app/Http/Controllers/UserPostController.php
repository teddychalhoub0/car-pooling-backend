<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserPostRequest;
use App\Models\UserPost;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserPostRepositoryInterface;
use Illuminate\Http\Request;

class UserPostController extends Controller
{
    /**
     * @var UserPostRepositoryInterface
     */
    private UserPostRepositoryInterface $userPostRepository;

    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * @param UserPostRepositoryInterface $userPostRepository
     */
    public function __construct(
        UserPostRepositoryInterface $userPostRepository,
        PostRepositoryInterface $postRepository
    )
    {
        $this->userPostRepository = $userPostRepository;
        $this->postRepository = $postRepository;
    }

    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        if(isset($request['post_user']) && $request['post_user']!=""){
            $user = auth("users")->user()->id;
            $userPosts = $this->userPostRepository->getByUserIdPostRelated($user);
        }else{
            $user = auth("users")->user()->id;
            $userPosts = $this->userPostRepository->getUserPostsByUserId($user);
        }


        if(!$userPosts->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'User post retrieved successfully',
                'data'=>$userPosts

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No user post found',
                'data'=>$userPosts
            ],404);
        }
    }

    /**
     *
     * Store a newly created resource in storage.
     *
     * @param AddUserPostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddUserPostRequest $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth("users")->user()->id;
//        dd($inputs['user_id'] );
        $userIdToPost = $this->postRepository->findUserIdToPost($request["post_id"]);
        if($inputs['user_id'] != $userIdToPost){


            $userPost = $this->userPostRepository->create($inputs);

            if($userPost){
                $postId = $userPost->id;

                $newUserPost = $this->userPostRepository->find($postId);
    //            dd($newUserPost);
                return response()->json([
                    "success" => 'true',
                    'message' => 'User post created successfully',
                    'data'=>$newUserPost
                ]);
            }else{
                return response()->json([
                    "success" => 'false',
                    'message' => 'User post wasn\'t created',
                    'data'=>$userPost
                ],400);
            }
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User can\'t tag along to yourself ',
            ],406);
        }
    }

    /**
     *
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $userPost = $this->userPostRepository->find($id);

        if($userPost){
            return response()->json([
                "success" => 'true',
                'message' => 'User post retrieved successfully',
                'data'=>$userPost
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No user post found',
                'data'=>$userPost
            ],404);
        }
    }

    /**
     *
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        $inputs = $request->all();

        $userPost = $this->userPostRepository->update($inputs,$id);

        if($userPost){
            return response()->json([
                "success" => 'true',
                'message' => 'User post updated successfully',
                'data'=>$userPost
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User post wasn\'t updated',
                'data'=>$userPost

            ],400);
        }
    }

    /**
     *
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $userPost = $this->userPostRepository->delete($id);

        if($userPost){
            return response()->json([
                "success" => 'true',
                'message' => 'User post deleted successfully',
                'data'=>$userPost
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User post wasn\'t deleted',
                'data'=>$userPost

            ],400);
        }
    }
}
