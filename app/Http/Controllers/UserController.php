<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repository\CarRepositoryInterface;
use App\Repository\CommentRepositoryInterface;
use App\Repository\LocationRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserPostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var LocationRepositoryInterface
     */
    private LocationRepositoryInterface $locationRepository;

    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface $postRepository;

    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $commentRepository;

    /**
     * @var UserPostRepositoryInterface
     */
    private UserPostRepositoryInterface $userPostRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param LocationRepositoryInterface $locationRepository
     * @param PostRepositoryInterface $postRepository
     * @param CarRepositoryInterface $carRepository
     * @param CommentRepositoryInterface $commentRepository
     * @param UserPostRepositoryInterface $userPostRepository
     */
    public function __construct
    (
        UserRepositoryInterface $userRepository,
        LocationRepositoryInterface $locationRepository,
        PostRepositoryInterface $postRepository,
        CarRepositoryInterface $carRepository,
        CommentRepositoryInterface $commentRepository,
        UserPostRepositoryInterface $userPostRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->locationRepository = $locationRepository;
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->carRepository = $carRepository;
        $this->userPostRepository=$userPostRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = $this->userRepository->all();

        if(!$users->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Users retrieved successfully',
                'data'=>$users

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Users found',
                'data'=>$users
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddUserRequest $request)
    {

        $inputs = $request->all();
        if($path = addImage($request->file('image'))){
            $inputs['image'] = $path;
        }

        if($path2 = addImage($request->file('car_license'))){
            $inputs['car_license'] = $path2;
        }

        $user = $this->userRepository->create($inputs);
        $user = $this->userRepository->find($user->id);

        $token = auth("users")->login($user);

        if($user){

            return response()->json([
                "success" => 'true',
                'message' => 'User created successfully',
                'data'=>$user,
                'token'=>$token
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User wasn\'t created',
                'data'=>$user
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if($user){
            return response()->json([
                "success" => 'true',
                'message' => 'User retrieved successfully',
                'data'=>$user
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No User found',
                'data'=>$user
            ],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try{
        $inputs = $request->all();
        $oldUser = $this->userRepository->find($id);
        if($oldUser){
            if( isset($request->image) && $request->hasFile("image")){
                if($oldUser->image){
                    if(deleteImage($oldUser->image)){
                        if($path = addImage($request->file('image'))){

                            $inputs['image'] = $path;

                        }else{
                            return response()->json([
                                "success" => 'false',
                                'message' => 'Problem updating image'
                            ],400);
                        }
                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Problem updating image'
                        ],400);
                    }
                }else{
                    if($path = addImage($request->file('image'))){
                        $inputs['image'] = $path;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Problem uploading image'
                        ],400);
                    }
                }
            }

            if(isset($request->user_license) && $request->hasFile("user_license") ){
                if(!$oldUser->user_license){
                    if($path2 = addImage($request->file('user_license'))){

                        $inputs['user_license'] = $path2;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Licence must be provided',
                        ],400);
                    }
                }else{
                    if(deleteImage($oldUser->user_license)){
                        if($path2 = addImage($request->file('user_license'))){

                            $inputs['user_license'] = $path2;

                        }else{
                            return response()->json([
                                "success" => 'false',
                                'message' => 'Licence must be provided',
                            ],400);
                        }

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Licence must be provided',
                        ],400);
                    }
                }
            }
//            dd(!$oldUser->car_license);
            if(isset($request->car_license) && $request->hasFile("car_license") ){
                if(!$oldUser->car_license){
//                    dd(addImage($request->file('car_license')));
                    if($path3 = addImage($request->file('car_license'))){

                        $inputs['car_license'] = $path3;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Licence must be provided',
                        ],400);
                    }
                }else{
                    if(deleteImage($oldUser->car_license)){
                        if($path3 = addImage($request->file('car_license'))){

                            $inputs['car_license'] = $path3;

                        }else{
                            return response()->json([
                                "success" => 'false',
                                'message' => 'Licence must be provided',
                            ],400);
                        }

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Car Licence must be provided',
                        ],400);
                    }
                }
            }

        }



        $user = $this->userRepository->update($inputs,$id);

        if($user){
            return response()->json([
                "success" => 'true',
                'message' => 'User updated successfully',
                'data'=>$user
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User wasn\'t updated',
                'data'=>$user

            ],400);
        }

        }catch(\Exception $error){
                $out = new \Symfony\Component\Console\Output\ConsoleOutput();
                $out->writeln($error);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $oldUser = $this->userRepository->find($id);
        $flag=0;
        if($oldUser){
            if($oldUser->image && deleteImage($oldUser->image)){
                $flag=1;
            }
            if($oldUser->user_license && deleteImage($oldUser->user_license)){
                $flag=1;
            }
            if($oldUser->car_license && deleteImage($oldUser->car_license)){
                $flag=1;
            }
            if($flag==1){
                $user = $this->userRepository->delete($id);
                $location = $this->locationRepository->deleteByUserId($id);
                $posts = $this->postRepository->deleteByUserId($id);
                $userPosts = $this->userPostRepository->deleteRelatedToUser($id);
                $comment = $this->commentRepository->deleteByUserId($id);
                $car = $this->carRepository->deleteByUserId($id);
            }
            else{
                return response()->json([
                    "success" => 'false',
                    'message' => 'Problem deleting image'
                ],400);
            }
        }


        if($user){
            return response()->json([
                "success" => 'true',
                'message' => 'User deleted successfully',
                'data'=>$user
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'User wasn\'t deleted',
                'data'=>$user
            ],400);
        }
    }
}
