<?php

namespace App\Http\Controllers;

use App\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;

class AuthUserController extends Controller
{


    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;


    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct
    (
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }
    /**
     *
     *User login function
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(){

        $credentials = request(['email', 'password']);

        if (!$token = auth('users')->attempt($credentials)) {
            return response()->json([
                'success'=>false ,
                'message' => 'Wrong username or password'
            ], 401);
        }

        $user = auth("users")->user();
        $newUser = $this->userRepository->find($user->id);

        return $this->createNewToken($token,$newUser);
    }

    /**
     * logout authenticated user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth("users")->logout();

        return response()->json([
            'success'=> true,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * refresh the token after it expires (2 weeks restriction)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        $user = auth("users")->user();
        $token = auth("users")->refresh();

        $newUser = $this->userRepository->find($user->id);
        return $this->createNewToken($token,$newUser);

    }

    /**
     * fetch authenticated user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $newUser = $this->userRepository->find(auth()->user("users")->id);

        return response()->json([
            'success'=>true,
            'message'=>'Fetched user successfully',
            'data'=>$newUser
        ]);
    }

    /**
     *
     * function that return a response if true for login, refresh
     *
     * @param $token
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token,$user){
        return response()->json([
            'success'=> true,
            'data'=>$user,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth("users")->factory()->getTTL() * 60,
            'message'=>"Login successfully",
        ]);
    }
}
