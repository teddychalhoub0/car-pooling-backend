<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\Admin;
use App\Repository\AdminRepositoryInterface;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * @var AdminRepositoryInterface
     */
    protected AdminRepositoryInterface $adminRepository;

    /**
     * @param AdminRepositoryInterface $adminRepository
     */
    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $admins = $this->adminRepository->all();

        if(!$admins->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Admins retrieved successfully',
                'data'=>$admins

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Admins found',
                'data'=>$admins
            ],404);
        }
    }

    /**
     *
     * Store a newly created resource in storage.
     *
     * @param AddAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddAdminRequest $request)
    {
        $inputs = $request->all();
        if($path = addImage($request->file('image'))){
            $inputs['image'] = $path;
        }
        $admin = $this->adminRepository->create($inputs);

        $token = auth("admins")->login($admin);

        if($admin){

            return response()->json([
                "success" => 'true',
                'message' => 'Admin created successfully',
                'data'=>$admin,
                'token'=>$token
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Admin wasn\'t created',
                'data'=>$admin
            ],400);
        }
    }

    /**
     *
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $admin = $this->adminRepository->find($id);

        if($admin){
            return response()->json([
                "success" => 'true',
                'message' => 'Admin retrieved successfully',
                'data'=>$admin
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Admin found',
                'data'=>$admin
            ],404);
        }
    }


    /**
     *
     * Update the specified resource in storage.
     *
     * @param UpdateAdminRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateAdminRequest $request, $id)
    {
        $inputs = $request->all();
        $oldAdmin = $this->adminRepository->find($id);

        if($oldAdmin && isset($request->image)){
            if($oldAdmin->image){
                if(deleteImage($oldAdmin->image)){
                    if($path = addImage($request->file('image'))){

                        $inputs['image'] = $path;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Problem updating image'
                        ],400);
                    }
                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem updating image'
                    ],400);
                }
            }else{
                if($path = addImage($request->file('image'))){
                    $inputs['image'] = $path;

                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem uploading image'
                    ],400);
                }
            }
        }

        $admin = $this->adminRepository->update($inputs,$id);

        if($admin){
            return response()->json([
                "success" => 'true',
                'message' => 'Admin updated successfully',
                'data'=>$admin
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Admin wasn\'t updated',
                'data'=>$admin

            ],400);
        }
    }

    /**
     *
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $oldAdmin = $this->adminRepository->find($id);

        if($oldAdmin && $oldAdmin->image){
            if(deleteImage($oldAdmin->image)){
                $admin = $this->adminRepository->delete($id);
            }else{
                return response()->json([
                    "success" => 'false',
                    'message' => 'Problem deleting image'
                ],400);
            }
        }else{
            $admin = $this->adminRepository->delete($id);
        }

        if($admin){
            return response()->json([
                "success" => 'true',
                'message' => 'Admin deleted successfully',
                'data'=>$admin
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Admin wasn\'t deleted',
                'data'=>$admin
            ],400);
        }
    }
}
