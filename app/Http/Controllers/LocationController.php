<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLocationRequest;
use App\Repository\LocationRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LocationController extends Controller
{
    /**
     * @var LocationRepositoryInterface
     */
    private LocationRepositoryInterface $locationRepository;

    /**
     * @param LocationRepositoryInterface $locationRepository
     */
    public function __construct(LocationRepositoryInterface $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $userId = auth("users")->user()->id;
        $locations = $this->locationRepository->findByUserId($userId);

        if(!$locations->isEmpty()){
            return response()->json([
                "success" => 'true',
                'message' => 'Locations retrieved successfully',
                'data'=>$locations

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No locations found',
                'data'=>$locations
            ],404);
        }
    }

    /**
     *
     * Store a newly created resource in storage.
     *
     * @param AddLocationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddLocationRequest $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth("users")->user()->id;

        $location = $this->locationRepository->create($inputs);

        if($location){

            return response()->json([
                "success" => 'true',
                'message' => 'Location created successfully',
                'data'=>$location
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Location wasn\'t created',
                'data'=>$location
            ],400);
        }
    }

    /**
     *
     *  Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $location = $this->locationRepository->find($id);

        if($location){
            return response()->json([
                "success" => 'true',
                'message' => 'Location retrieved successfully',
                'data'=>$location
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No location found',
                'data'=>$location
            ],404);
        }
    }

    /**
     *
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        $inputs = $request->all();

        $location = $this->locationRepository->update($inputs,$id);

        if($location){
            return response()->json([
                "success" => 'true',
                'message' => 'Location updated successfully',
                'data'=>$location
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Location wasn\'t updated',
                'data'=>$location

            ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $location = $this->locationRepository->delete($id);

        if($location){
            return response()->json([
                "success" => 'true',
                'message' => 'Location deleted successfully',
                'data'=>$location
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Location wasn\'t deleted',
                'data'=>$location

            ],400);
        }
    }

    /**
     *
     * get location by lat and long
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentLocation(Request $request){

        $resposne = Http::get('http://dev.virtualearth.net/REST/v1/Locations/'.$request['lat'].','.$request['long'].'?key='.env('BING_MAP_KEY'));

        if(json_decode($resposne)->statusCode === 200){
            $values2 = json_decode($resposne)->resourceSets[0]->resources[0];

            return response()->json([
                'success'=>true,
                'message'=>'successfully retrieved',
                'data'=>$values2
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Not retrieved successfully'
            ],400);
        }
    }

    /**
     * provide autosuggestion for location
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuggestedLocation(Request $request){

        $resposne = Http::get('http://dev.virtualearth.net/REST/v1/Autosuggest?query='.$request['query'].'&key='.env('BING_MAP_KEY'));

        if(json_decode($resposne)->statusCode === 200){
            $values = json_decode($resposne)->resourceSets[0]->resources[0]->value;
            $values2 = [];
            foreach ($values as $value){
                if($value->address->countryRegion == "Lebanon"){
                    array_push($values2,$value);
                }
            }

            return response()->json([
                'success'=>true,
                'message'=>'successfully retrieved',
                'data'=>$values2
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Not retrieved successfully'
            ],400);
        }

    }
}
