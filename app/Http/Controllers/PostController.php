<?php

namespace App\Http\Controllers;

use App\Events\PostEvent;
use App\Http\Requests\AddPostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserPostRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{

    /**
     * @var PostRepositoryInterface
     */
    private PostRepositoryInterface  $postRepository;

    /**
     * @var UserPostRepositoryInterface
     */
    private UserPostRepositoryInterface  $userPostRepository;

    public function __construct(PostRepositoryInterface $postRepository,UserPostRepositoryInterface  $userPostRepository)
    {
        $this->postRepository = $postRepository;
        $this->userPostRepository = $userPostRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if(isset($request['search']) && $request['search'] != "" && $request['departure']==="true"){
            $posts = $this->postRepository->searchByDeparture($request['search']);
        }else if(isset($request['search']) && $request['search'] != ""){
            $posts = $this->postRepository->searchByDestination($request['search']);
        }else{
            $posts = $this->postRepository->all();
        }

        if(!$posts->isEmpty()){
            return response()->json([
               "success" => 'true',
                'message' => 'Posts retrieved successfully',
                'data'=>$posts

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Posts found',
                'data'=>$posts
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     *
     * @param AddPostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddPostRequest $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth("users")->user()->id;
        if($path = addImage($request->file('image'))){
            $inputs['image'] = $path;
        }
       $post = $this->postRepository->create($inputs);

       if($post){
           event(new PostEvent($post));
           return response()->json([
               "success" => 'true',
               'message' => 'Post created successfully',
               'data'=>$post
           ]);
       }else{
           return response()->json([
               "success" => 'false',
               'message' => 'Posts wasn\'t created',
               'data'=>$post

           ],400);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);

        if($post){
            return response()->json([
                "success" => 'true',
                'message' => 'Post retrieved successfully',
                'data'=>$post

            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'No Post found',
                'data'=>$post
            ],404);
        }
    }


    /**
     *
     *  Update the specified resource in storage.
     *
     * @param UpdatePostRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePostRequest $request,$id)
    {

        $inputs = $request->all();
        $oldPost = $this->postRepository->find($id);

        if($oldPost && isset($request->image)){
            if($oldPost->image){
                if(deleteImage($oldPost->image)){
                    if($path = addImage($request->file('image'))){

                        $inputs['image'] = $path;

                    }else{
                        return response()->json([
                            "success" => 'false',
                            'message' => 'Problem updating image'
                        ],400);
                    }
                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem updating image'
                    ],400);
                }
            }else{
                if($path = addImage($request->file('image'))){
                    $inputs['image'] = $path;

                }else{
                    return response()->json([
                        "success" => 'false',
                        'message' => 'Problem uploading image'
                    ],400);
                }
            }
        }


        $post = $this->postRepository->update($inputs,$id);

        if($post){
            return response()->json([
                "success" => 'true',
                'message' => 'Post updated successfully',
                'data'=>$post
            ]);
        }else{
            return response()->json([
                "success" => 'false',
                'message' => 'Posts wasn\'t updated',
                'data'=>$post

            ],400);
        }
    }

    /**
     *
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $oldPost = $this->postRepository->find($id);

        if ($oldPost && $oldPost->image) {
            if (deleteImage($oldPost->image)) {
                $post = $this->postRepository->delete($id);
                $userPost = $this->userPostRepository->deleteRelatedToPost($id);
            } else {
                return response()->json([
                    "success" => 'false',
                    'message' => 'Problem deleting image'
                ], 400);
            }
        } else {
            $post = $this->postRepository->delete($id);
            $userPost = $this->userPostRepository->deleteRelatedToPost($id);
        }

        if ($post) {
            return response()->json([
                "success" => 'true',
                'message' => 'Post deleted successfully',
                'data' => $post
            ]);
        } else {
            return response()->json([
                "success" => 'false',
                'message' => 'Post wasn\'t deleted',
                'data' => $post
            ], 400);
        }
    }
}
