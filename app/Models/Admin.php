<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable=[
        'name',
        'username',
        'email',
        'password',
        'image'
    ];

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
       return [];
    }

    /**
     * @param $pass
     */
    public function setPasswordAttribute($pass)
    {
        if ( !empty($pass) ) {
            $this->attributes['password'] = bcrypt($pass);
        }
    }
}
