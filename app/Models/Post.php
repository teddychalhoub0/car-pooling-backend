<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $fillable=[
        'title',
        'content',
        'nb_of_pass',
        'departure_time',
        'estimated_time',
        'departure_place',
        'destination_place',
        'image',
        'user_id',
        'car_id',
        'default_route'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function user_post(){
        return $this->hasMany(UserPost::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }

}
