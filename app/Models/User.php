<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
        'username',
        'mobile',
        'about_me',
        'user_license',
        'car_license'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param $pass
     */
    public function setPasswordAttribute($pass)
    {
        if ( !empty($pass) ) {
            $this->attributes['password'] = bcrypt($pass);
        }
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function user_post(){
        return $this->hasMany(UserPost::class);
    }

    public function locations(){
        return $this->hasMany(Location::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function cars(){
        return $this->hasMany(Car::class);
    }
}
