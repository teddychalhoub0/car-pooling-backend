<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $fillable=[
        'image',
        'car_plate_number',
        'car_color',
        'car_model',
        'nb_seats',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
