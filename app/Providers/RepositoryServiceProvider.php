<?php

namespace App\Providers;

use App\Repository\AdminRepositoryInterface;
use App\Repository\CarRepositoryInterface;
use App\Repository\CommentRepositoryInterface;
use App\Repository\Eloquent\AdminRepository;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\CarRepository;
use App\Repository\Eloquent\CommentRepository;
use App\Repository\Eloquent\LocationRepository;
use App\Repository\Eloquent\PostRepository;
use App\Repository\Eloquent\UserPostRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\EloquentBaseRepositoryInterface;
use App\Repository\LocationRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserPostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentBaseRepositoryInterface::class,BaseRepository::class);
        $this->app->bind(PostRepositoryInterface::class,PostRepository::class);
        $this->app->bind(LocationRepositoryInterface::class,LocationRepository::class);
        $this->app->bind(UserRepositoryInterface::class,UserRepository::class);
        $this->app->bind(AdminRepositoryInterface::class,AdminRepository::class);
        $this->app->bind(UserPostRepositoryInterface::class,UserPostRepository::class);
        $this->app->bind(CommentRepositoryInterface::class,CommentRepository::class);
        $this->app->bind(CarRepositoryInterface::class,CarRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
