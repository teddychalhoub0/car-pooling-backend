﻿<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthAdminController;
use App\Http\Controllers\AuthUserController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Posts Crud apis

// Admins Crud apis
Route::group(['prefix'=>'admins'],function() {
    Route::post("/login", [AuthAdminController::class, "login"]);

    Route::group(['middleware' => ['jwt.verify:admins']], function () {
        // admin post
        Route::apiResource('/posts', PostController::class);
        // Admin crud api
        Route::apiResource('/details', AdminController::class);
        Route::post("/logout", [AuthAdminController::class, "logout"]);
    });
});

// Users Crud apis
Route::group(['prefix'=>'users'],function(){
    Route::post("/login",[AuthUserController::class,"login"]);
    Route::post("/register",[UserController::class,"store"]);
    Route::post("/refreshToken",[AuthUserController::class,"refreshToken"]);

    Route::group([ 'middleware' => ['jwt.verify:users']], function () {
            // User crud api
            Route::apiResource('/details',UserController::class);
            // Users post
            Route::apiResource('/posts',PostController::class);
             // Users location
            Route::apiResource('/locations',LocationController::class);
            // User post crud api
            Route::apiResource('/user-post',UserPostController::class);
            // Comment crud api
             Route::apiResource('/comments',CommentController::class);
             Route::get('/user-comments',[CommentController::class,"getCommentsByUserId"]);
            // Car crud api
            Route::apiResource('/cars',CarController::class);
            Route::post("/logout",[AuthUserController::class,"logout"]);
            Route::get("/auth-user",[AuthUserController::class,"me"]);
            Route::post("/getSuggestions",[LocationController::class,'getSuggestedLocation']);
            Route::post("/getLocation",[LocationController::class,'getCurrentLocation']);
    });
});

